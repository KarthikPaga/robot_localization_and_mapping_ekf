function Jacobian = landmarks_cov_globalFrame (measurements, x_r)
    beta = [];
    range = [];
    idx = 1;
    while idx <= length(measurements)
        beta = horzcat(beta, measurements(idx));
        range = horzcat(range, measurements(idx + 1));
        idx = idx +2;
    end
    
    beta = beta';
    range = range';
    
    Jacobian = [];
    
    for i = 1:length(beta)
%         C_t = [cos(beta(i) -sin(beta(i)); sin(beta(i)) cos(beta(i))];
        Z = [range(i)*cos(wrapToPi(x_r(3) + beta(i))); range(i)*sin(wrapToPi(x_r(3) + beta(i)))];
        x_l = x_r(1:2) + Z;
        % forming the Jacobian of l_x and l_y wrt r and Beta
        dx = [cos(wrapToPi(x_r(3) + beta(i))), -1*range(i)*sin(wrapToPi(x_r(3) + beta(i)))];
        dy = [sin(wrapToPi(x_r(3) + beta(i))), range(i)*cos(wrapToPi(x_r(3) + beta(i)))];
        Jacobian = vertcat(Jacobian, [dx; dy]);
    end
        
end