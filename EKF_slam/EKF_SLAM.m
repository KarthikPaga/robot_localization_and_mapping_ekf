%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  16833 Robot Localization and Mapping  % 
%  Assignment #2                         %
%  EKF-SLAM                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
close all;

%==== TEST: Setup uncertianty parameters (try different values!) ===
sig_x = 0.5;
sig_y = 0.1;
sig_alpha = 0.1;
sig_beta = 0.01;
sig_r = 0.08;

%==== Generate sigma^2 from sigma ===
sig_x2 = sig_x^2;
sig_y2 = sig_y^2;
sig_alpha2 = sig_alpha^2;
sig_beta2 = sig_beta^2;
sig_r2 = sig_r^2;

%==== Open data file ====
fid = fopen('../data/data.txt');

%==== Read first measurement data ====
tline = fgets(fid);
arr = str2num(tline);
measure = arr';
t = 1;
 
%==== Setup control and measurement covariances ===
control_cov = diag([sig_x2, sig_y2, sig_alpha2]); %R
measure_cov = diag([sig_beta2, sig_r2]); %Q

%==== Setup initial pose vector and pose uncertainty ====
pose = [0 ; 0 ; 0];
pose_cov = diag([0.02^2, 0.02^2, 0.1^2]);

%==== TODO: Setup initial landmark vector landmark[] and covariance matrix landmark_cov[] ====
%==== (Hint: use initial pose with uncertainty and first measurement) ====

% Write your code here...
landmark = landmarks_globalFrame(measure, pose);
% Set these to some initial random config. 
% Each landmark has (l_x, l_y)
% 12x 2 matrix - Jacobian of [l_x; l_y] wrt r and beta for all the 2 points
J_r_Beta = landmarks_cov_globalFrame(measure, pose);
landmark_cov = J_r_Beta * diag([measure_cov(4), measure_cov(1)]) * J_r_Beta';




% ------------------
k = length(landmark)/2;

%==== Setup state vector x with pose and landmark vector ====
x = [pose ; landmark];

%==== Setup covariance matrix P with pose and landmark covariances ====
P = [pose_cov zeros(3, 2*k) ; zeros(2*k, 3) landmark_cov];

%==== Plot initial state and conariance ====
last_x = x;
drawTrajAndMap(x, last_x, P, 0);

%==== Read control data ====
tline = fgets(fid);
while ischar(tline)
    arr = str2num(tline);
    d = arr(1);
    alpha = arr(2);
    
    %==== TODO: Predict Step ====
    %==== (Notice: predict state x_pre[] and covariance P_pre[] using input control data and control_cov[]) ====
    
    % Write your code here...
    F = vertcat(eye(3), zeros(length(landmark), 3));
    F = F'; %notation in the reference
    
    % [dcos(theta), dsin(theta), alpha]    
    u = [d*cos(x(3)); d*sin(x(3)); alpha];
    x_pre = x + (F' * u);
    x_pre(3) = wrapToPi(x_pre(3));
    
    % P_pre = G * Cov_pose * G' + process_noise
    G= eye(2*k + 3) + (F'*[0, 0, -d*(sin(x(3))); 0, 0, d*(cos(x(3))); 0, 0, 0]* F);
    %Covariance - propogating uncertainity in the pose prediction
    P_pre = G*P*G' + (F' * control_cov * F);
    
    %==== Draw predicted state x_pre[] and covariance P_pre[] ====
    drawTrajPre(x_pre, P_pre);
%     waitforbuttonpress;
    
    
    %==== Read measurement data ====
    tline = fgets(fid);
    arr = str2num(tline);
    measure = arr';
    
    %==== TODO: Update Step ====
    %==== (Notice: update state x[] and covariance P[] using input measurement data and measure_cov[]) ====
    
    % Write your code here...
    for j = 1:k
        landmark = x_pre(4:end);
        delta = [landmark((2*j)-1) - x_pre(1); landmark(2*j) - x_pre(2)];
        q = delta' * delta;
        z_hat = [sqrt(q); wrapToPi(atan2(delta(2), delta(1)) - x_pre(3))];
        F_j = horzcat(vertcat(eye(3), zeros(2,3)), zeros(5, 2*j-2), vertcat(zeros(3,2),eye(2)), zeros(5, 2*k-2*j));
%         F_j = horzcat(vertcat(eye(3), zeros(2,3)), zeros(5, 2*k-2), vertcat(zeros(3,2),eye(2)));
        H_t = (1/q)*([-1*sqrt(q)*delta(1), -1*sqrt(q)*delta(2), 0, sqrt(q)*delta(1), sqrt(q)*delta(2); delta(2), -1*delta(1), -1*q, -1*delta(2), delta(1)]) * F_j;
        K_t = P_pre * H_t'*(inv((H_t*P_pre*H_t') + measure_cov));
        z_t = [measure((2*j)); measure((2*j) -1)];
        if (wrapToPi(z_t(2))) < 0 
            z_t(2) = -1 * wrapToPi(z_t(2)) + 3.14;
        end
        x_pre = x_pre + K_t*(z_t - z_hat);
        x_pre(3) = wrapToPi(x_pre(3));
        P_pre = ( eye(2*k +3) - (K_t * H_t)) * P_pre; 
    end
    x_pre(3) = wrapToPi(x_pre(3));
    x = x_pre;
    P = P_pre;
    
    
    %==== Plot ====   
    drawTrajAndMap(x, last_x, P, t);
%     waitforbuttonpress;
    last_x = x;
    
    %==== Iteration & read next control data ===
    t = t + 1;
    tline = fgets(fid);
end

%==== EVAL: Plot ground truth landmarks ====

% Write your code here...
plot([3,3,7,7,11,11], [6,12,8,14,6,12], 'b+');
    

%==== Close data file ====
fclose(fid);
