function landmarks = landmarks_globalFrame (measurements, x_r)
    beta = [];
    range = [];
    idx = 1;
    while idx <= length(measurements)
        beta = horzcat(beta, measurements(idx));
        range = horzcat(range, measurements(idx + 1));
        idx = idx +2;
    end
    
    beta = beta';
    range = range';
    
    landmarks = [];
    
    for i = 1:length(beta)
%         C_t = [cos(beta(i) -sin(beta(i)); sin(beta(i)) cos(beta(i))];
        Z = [range(i)*cos(wrapToPi(x_r(3) + beta(i))); range(i)*sin(wrapToPi(x_r(3) + beta(i)))];
        x_l = x_r(1:2) + Z;
        landmarks = vertcat(landmarks, x_l);
    end
        
end